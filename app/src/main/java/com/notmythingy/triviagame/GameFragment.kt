package com.notmythingy.triviagame

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.notmythingy.triviagame.databinding.FragmentGameBinding

class GameFragment : Fragment() {
    data class Question(
        val text: String,
        val answers: List<String>
    )

    // The first answer is the correct one. Answers are randomized before showing the text.
    // All questions must have four answers.
    private val questions: MutableList<Question> = mutableListOf(
        Question(
            text = "Minkä planeetan ympärillä on hohtavat jää- ja pölyrenkaat?",
            answers = listOf("Saturnuksen", "Merkuriuksen", "Maan", "Kryptonin")
        ),
        Question(
            text = "Mitä kuun pinnalla on?",
            answers = listOf("Kraattereita", "Metsää", "Hämähäkkejä", "Juustoa")
        ),
        Question(
            text = "Montako kuuta Maalla on?",
            answers = listOf("1", "10", "3", "Ei yhtään, doh!")
        ),
        Question(
            text = "Mitä auringossa ei ole?",
            answers = listOf("Happea", "Heliumia", "Vetyä", "Lämmin")
        ),
        Question(
            text = "Minkä niminen koira on käynyt avaruudessa?",
            answers = listOf("Laika", "Pluto", "Musti", "Turre")
        ),
        Question(
            text = "Mitä astronauteilla on säiliössä, kun he menevät ulos avaruusaluksesta?",
            answers = listOf("Ilmaa (happea)", "Vettä", "Ruokaa", "Vaihtovaatteet")
        ),
        Question(
            text = "Avaruutta sanotaan myös...",
            answers = listOf("Universumiksi", "Linnunradaksi", "Maapalloksi", "Planetaarioksi"
            )
        ),
        Question(
            text = "Mikä on nimeltään avaruusalus, joka kiertää planeettoja ja lähettää signaaleja maahan?",
            answers = listOf("Satelliitti", "Komeetta", "Planeetta", "Tesla")
        ),
        Question(
            text = "Mitä planeetta EI peitä paksu myrkyllinen pilvikerros?",
            answers = listOf("Maata", "Uranusta", "Neptunusta", "WASP-39b")
        ),
        Question(
            text = "Mikä on valonnopeus?",
            answers = listOf("300 000 kilometriä sekunnissa", "30 kilometriä sekunnissa", "3000 kilometriä sekunnissa", "300 kilometriä tunnissa")
        )
    )

    lateinit var currentQuestion: Question
    lateinit var answers: MutableList<String>
    private var questionIndex = 0
    private val numQuestions = ((questions.size + 1) / 2).coerceAtMost(3)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentGameBinding>(
            inflater, R.layout.fragment_game, container, false
        )

        // Shuffles the questions and sets the question index to the first question.
        randomizeQuestions()

        // Bind this fragment to the layout
        binding.game = this

        binding.submitButton.setOnClickListener @Suppress("UNUSED_ANONYMOUS_PARAMETER")
        { view: View ->
            val checkedId = binding.questionRadioGroup.checkedRadioButtonId
            // Do nothing if nothing is checked (id == -1)
            if (-1 != checkedId) {
                var answerIndex = 0
                when (checkedId) {
                    R.id.secondAnswerRadioButton -> answerIndex = 1
                    R.id.thirdAnswerRadioButton -> answerIndex = 2
                    R.id.fourthAnswerRadioButton -> answerIndex = 3
                }
                // The first answer in the original question is always the correct one
                if (answers[answerIndex] == currentQuestion.answers[0]) {
                    questionIndex++
                    if (questionIndex < numQuestions) {
                        currentQuestion = questions[questionIndex]
                        setQuestion()
                        binding.invalidateAll()
                    } else {
                        view.findNavController()
                            .navigate(R.id.action_gameFragment_to_gameWinnerFragment)
                    }
                } else {
                    view.findNavController()
                        .navigate(R.id.action_gameFragment_to_gameOverFragment)
                }
            }
        }
        return binding.root
    }

    // Set random question
    private fun randomizeQuestions() {
        questions.shuffle()
        questionIndex = 0
        setQuestion()
    }

    // Sets the question and randomizes the answers.
    private fun setQuestion() {
        currentQuestion = questions[questionIndex]
        // randomize the answers into a copy of the array
        answers = currentQuestion.answers.toMutableList()
        // and shuffle them
        answers.shuffle()
        (activity as AppCompatActivity).supportActionBar?.title =
            getString(R.string.title_android_trivia_question, questionIndex + 1, numQuestions)
    }
}