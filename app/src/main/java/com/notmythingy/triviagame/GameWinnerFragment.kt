package com.notmythingy.triviagame

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.notmythingy.triviagame.databinding.FragmentGameWinnerBinding


class GameWinnerFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val binding: FragmentGameWinnerBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_game_winner, container, false)
        binding.nextMatchButton.setOnClickListener { view: View->
            view.findNavController().navigate(R.id.action_gameWinnerFragment_to_gameFragment)
        }
        return binding.root
    }
}